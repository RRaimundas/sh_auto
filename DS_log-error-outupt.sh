# vi /opt/scripts/os-log-alert.sh

#!/bin/bash

#Set the variable which equal to zero

prev_count=0

count=$(grep -i "`date --date='yesterday' '+%b %e'`" /home/fsp/servers/mule-standalone-3.8.1/logs/dsview-error.log | egrep -wi 'ERROR' /home/fsp/servers/mule-standalone-3.8.1/logs/dsview-error.log | wc -l)

if [ "$prev_count" -lt "$count" ] ; then

# Send a mail to given email id when errors found in log

SUBJECT="WARNING: Errors found in log on  "`date --date='yesterday' '+%b %e'`""

# This is a temp file, which is created to store the email message.

MESSAGE="/tmp/logs.txt"

TO="system1@vpb.gov.lt"
TOENDUSER="raimundas.rimkus@vpb.gov.lt"
#TYPE="ssmtp"


echo "+------------------------------------------------------------------------------------+" >> $MESSAGE
echo  "Hostname: `hostname`" >> $MESSAGE
echo -e "\n" >> $MESSAGE
echo $SUBJECT >> $MESSAGE
echo "Errors are in /home/fsp/servers/mule-standalone-3.8.1/logs/dsview-error.log. Please Check with Linux admin." >> $MESSAGE
echo -e "\n" >> $MESSAGE
echo "Error messages in the log file as below" >> $MESSAGE

echo "+------------------------------------------------------------------------------------+" >> $MESSAGE

grep -i "`date --date='yesterday' '+%b %e'`" /home/fsp/servers/mule-standalone-3.8.1/logs/dsview-error.log | egrep -A 1 -iw 'ERROR.*' /home/fsp/servers/mule-standalone-3.8.1/logs/dsview-error.log | awk '{print}' >>  $MESSAGE

cat $MESSAGE | mail -s "$SUBJECT" -r "$TO" -t "$TOENDUSER"

rm $MESSAGE

fi
