#!/bin/bash
case "$1" in 'jboss'|'wildfly')
	echo "wildfly redeploy"
        rm -rf /opt/bo/wildfly/standalone/data/content/*
        rm -rf /opt/bo/wildfly/standalone/data/timer-service-data/*
        rm -rf /opt/bo/wildfly/standalone/tmp/*
        rm -rf /opt/bo/wildfly/standalone/deployments/*ear*
        cp core/BatchMgmt/WIPOImportBatchService/WipoServiceEAR/target/sp-bo-core-batch.ear /opt/bo/wildfly/standalone/deployments
        cp core/DocumentMgmt/DocumentClient/CMISDocumentClient/CMISDocumentClientServiceEAR/target/sp-bo-core-document-client-cmis.ear /opt/bo/wildfly/standalone/deployments
        cp core/Audit/AuditServiceEAR/target/sp-bo-core-audit.ear /opt/bo/wildfly/standalone/deployments
        cp core/ConfigurationMgmt/ConfigurationServiceEAR/target/sp-bo-core-configuration.ear /opt/bo/wildfly/standalone/deployments
	    cp core/DocumentMgmt/DocumentServiceEAR/target/sp-bo-core-document.ear /opt/bo/wildfly/standalone/deployments
        cp core/DossierMgmt/DossierAnnotation/DossierAnnotationEAR/target/sp-bo-core-annotation.ear /opt/bo/wildfly/standalone/deployments
        cp core/DossierMgmt/DossierService/DossierServiceEAR/target/sp-bo-core-dossier.ear /opt/bo/wildfly/standalone/deployments        
        cp core/FeeMgmt/FeeServiceEAR/target/sp-bo-core-fee.ear /opt/bo/wildfly/standalone/deployments
        cp core/DocumentMgmt/DocumentClient/JCRDocumentClient/JCRDocumentClientServiceEAR/target/sp-bo-core-document-client-jcr.ear /opt/bo/wildfly/standalone/deployments
        cp core/LetterMgmt/LetterService/LetterServiceEAR/target/sp-bo-core-letter.ear /opt/bo/wildfly/standalone/deployments
        cp core/LetterMgmt/LetterTemplateService/LetterTemplateEAR/target/sp-bo-core-lettertemplate.ear /opt/bo/wildfly/standalone/deployments
        cp core/DossierMgmt/LocarnoClassificationService/LocarnoClassificationServiceEAR/target/sp-bo-core-locarno.ear /opt/bo/wildfly/standalone/deployments
        cp core/WorkflowMgmt/NotifyPresentationServiceEAR/target/sp-bo-core-workflow-notify.ear /opt/bo/wildfly/standalone/deployments
        cp core/PersonMgmt/PersonServiceEAR/target/sp-bo-core-person.ear /opt/bo/wildfly/standalone/deployments
        cp core/PublicationMgmt/PublicationServiceEAR/target/sp-bo-core-publication.ear /opt/bo/wildfly/standalone/deployments
        cp core/Rules/RulesServiceEAR/target/sp-bo-core-rules.ear /opt/bo/wildfly/standalone/deployments
        cp core/SearchMgmt/SearchServiceEAR/target/sp-bo-core-search.ear /opt/bo/wildfly/standalone/deployments
        cp core/WorkflowMgmt/TaskServiceEAR/target/sp-bo-core-workflow-task.ear /opt/bo/wildfly/standalone/deployments
        cp core/UserPrefsMgmt/UserPreferencesServiceEAR/target/sp-bo-core-user-prefs.ear /opt/bo/wildfly/standalone/deployments
        cp core/ClassificationMgmt/ViennaClassificationService/ViennaClassificationServiceEAR/target/sp-bo-core-vienna.ear /opt/bo/wildfly/standalone/deployments
        cp core/ESignature/ESignatureServiceEAR/target/sp-bo-core-esignature.ear /opt/bo/wildfly/standalone/deployments
        cp core/Numbering/NumberingEAR/target/sp-bo-numbering.ear /opt/bo/wildfly/standalone/deployments
        cp core/DocumentMgmt/DocumentQueueEAR/target/sp-bo-core-document-queue.ear /opt/bo/wildfly/standalone/deployments
        cp core/EmailServiceMgmt/EmailServiceEAR/target/sp-bo-core-email.ear /opt/bo/wildfly/standalone/deployments
    #Previously Tomcat
        cp ui/FrontEnd/target/backoffice-fe.war /opt/bo/wildfly/standalone/deployments
        cp ui/BackEnd/target/sp-bo-ui-backend.war /opt/bo/wildfly/standalone/deployments

    #config files
        cp -r conf/releases/target/wildfly/standalone-full.xml /opt/bo/wildfly/standalone/configuration
;;

'mule')
	echo "Mule redeploy"
        cp integration/mule/CorrespondenceProvider/target/sp-bo-integration-correspondence.zip /opt/bo/mule/apps/
        cp integration/mule/DossierProvider/target/sp-bo-integration-dossier.zip /opt/bo/mule/apps/
        cp integration/mule/FeeProvider/target/sp-bo-integration-fee.zip /opt/bo/mule/apps/
        cp integration/mule/IntegrationCommon/target/IntegrationCommon.jar /opt/bo/mule/apps/
        cp integration/mule/MockCorrespondenceAdapter/target/sp-bo-integration-correspondence-mock.zip /opt/bo/mule/apps/
        cp integration/mule/MockFeeAdapter/target/sp-bo-integration-fee-mock.zip /opt/bo/mule/apps/
        cp integration/mule/MockNumberingAdapter/target/sp-bo-integration-numbering-mock.zip /opt/bo/mule/apps/
        cp integration/mule/MockPersonAdapter/target/sp-bo-integration-person-mock.zip /opt/bo/mule/apps/
        cp integration/mule/MockPublicationAdapter/target/sp-bo-integration-publication-mock.zip /opt/bo/mule/apps/
        cp integration/mule/PersonProvider/target/sp-bo-integration-person.zip /opt/bo/mule/apps/
        cp integration/mule/PublicationProvider/target/sp-bo-integration-publication.zip /opt/bo/mule/apps/
        cp integration/mule/WorkflowProvider/target/sp-bo-integration-workflow.zip /opt/bo/mule/apps/
;;

'workflows')
	echo "workflows redeploy"
        #this is neccessary to get rid of old flows
            rm -rf /opt/bo/workflows/*
        	find conf/releases/target/bpmn/ -name '*.bpmn*' | xargs -i cp {} /opt/bo/workflows/
        	find conf/releases/Lithuania/src/main/resources/bpmn/ -name '*.bpmn*' | xargs -i cp {} /opt/bo/workflows/
;;

'tomcat')
	echo "Tomcat redeploy"
	rm -rf /opt/bo/tomcat/temp/*
	rm -rf /opt/bo/tomcat/work/*
	rm -rf /opt/bo/tomcat/webapps/sp-bo-ui-backend*
	rm -rf /opt/bo/tomcat/webapps/backoffice-fe*
	rm -rf /opt/bo/tomcat/webapps/*openam*
	cp ui/BackEnd/target/sp-bo-ui-backend.war /opt/bo/tomcat/webapps
	cp -R ui/FrontEnd/target/build /opt/bo/tomcat/webapps/backoffice-fe
	cp integration/ui-mock/MockOpenAMProvider/target/mock-openam.war /opt/bo/tomcat/webapps
;;
'database')
	echo "Database recreate"
	DB_BACKOFFICE="backoffice"
	DB_ACTIVITI="activiti"
	DB_USERNAME="bo"
	DB_PASSWORD="bo"

	echo "Database activiti recreate"
	echo "Database DROP activiti"
	mysql -u $DB_USERNAME -p$DB_PASSWORD  -h 127.0.0.1 $DB_ACTIVITI < conf/releases/target/sql/mysql/activiti/drop-activiti-schema.sql
	echo "-- create-activiti-schema.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_ACTIVITI < conf/releases/target/sql/mysql/activiti/create-activiti-schema.sql
	echo "-- add-activiti-bo-indexes.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_ACTIVITI < conf/releases/target/sql/mysql/activiti/add-activiti-bo-indexes.sql

	echo "Database backoffice recreate"
	echo "-- drop-fsp-schema.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/mysql/drop-fsp-schema.sql
	echo "-- drop-fsp-bo-schema.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/mysql/drop-fsp-bo-schema.sql
	echo "-- 01_create-fsp-schema.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/mysql/01_create-fsp-schema.sql
	echo "-- 02_create-fsp-bo-schema.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/mysql/02_create-fsp-bo-schema.sql
	echo "-- 03_fsp-bo-application-params-loading.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/common/03_fsp-bo-application-params-loading.sql
	echo "-- 04_system-params-loading.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/common/04_system-params-loading.sql
	echo "-- 05_system-params-loading-tmbo.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/common/05_system-params-loading-tmbo.sql
	echo "-- 06_fsp-bo-locarno-data-loading.sql"
	mysql -u $DB_USERNAME -p$DB_PASSWORD -h 127.0.0.1 $DB_BACKOFFICE < conf/releases/target/sql/common/06_fsp-bo-locarno-data-loading.sql

;;
'buildprod')
	echo "Building code"
	mvn clean install -DboDev -Pprod -Dmaven.test.skip=true -Dldap.disabled=false -s settings.xml
;;
'buildtest')
	echo "Building code"
	mvn clean install -DboDev -Ptest -Dmaven.test.skip=true -Dldap.disabled=false -s settings.xml
;;
'builddev')
	echo "Building code"
	mvn clean install -DboDev -Pdev -Dmaven.test.skip=true -Dldap.disabled=false -s settings.xml
;;
'buildlocal')
	echo "Building code"
	mvn clean install -DboDev -Plocal -Dmaven.test.skip=true -Dldap.disabled=true -s settings.xml
;;
'test')
	echo "Building with TESTing"
	mvn clean install -DboDev -Dmaven.test.skip=false -Dldap.disabled=true -s settings.xml
;;
*)
	echo "Usage: ./build.sh tomcat|jboss|wildfly|mule|database|workflows|buildprod|buildtest|builddev|buildlocal|test"
;;
esac

tput bel

