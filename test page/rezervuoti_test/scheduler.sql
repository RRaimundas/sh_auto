-- phpMyAdmin SQL Dump
-- version 4.7.5
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Jun 25, 2019 at 07:47 PM
-- Server version: 5.7.20
-- PHP Version: 7.1.7

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `scheduler`
--

-- --------------------------------------------------------

--
-- Table structure for table `events`
--

CREATE TABLE `events` (
  `id` int(11) NOT NULL,
  `name` text COLLATE utf8_lithuanian_ci,
  `start` datetime DEFAULT NULL,
  `end` datetime DEFAULT NULL,
  `resource` varchar(30) COLLATE utf8_lithuanian_ci DEFAULT NULL,
  `ps` varchar(10) COLLATE utf8_lithuanian_ci DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_lithuanian_ci;

--
-- Dumping data for table `events`
--

INSERT INTO `events` (`id`, `name`, `start`, `end`, `resource`, `ps`) VALUES
(13, 'Susitikimas su ATEA <br /> <br />Skaitykla <br /> <br />Brigita', '2019-07-31 11:00:00', '2019-07-31 14:00:00', NULL, ''),
(21, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-05-21 11:30:00', '2019-05-21 16:30:00', NULL, ''),
(22, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-05-28 11:30:00', '2019-05-28 14:30:00', NULL, ''),
(23, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-05-28 13:30:00', '2019-05-28 17:00:00', NULL, ''),
(24, '<b>Anglu k.</b>  <br />Skaitykla (Vincas)', '2019-05-30 12:00:00', '2019-05-30 15:30:00', NULL, ''),
(25, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-07-29 11:30:00', '2019-07-29 13:00:00', NULL, ''),
(49, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-06-25 12:00:00', '2019-06-25 15:00:00', NULL, '123'),
(50, '<b>Susitikimas su ATEA</b>  <br />Skaitykla (Brigita)', '2019-06-27 12:00:00', '2019-06-27 14:00:00', NULL, '123');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `events`
--
ALTER TABLE `events`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `events`
--
ALTER TABLE `events`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
