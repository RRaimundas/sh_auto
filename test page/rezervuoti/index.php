<!DOCTYPE html>
<html>
<head>
    <title>VPB patalpų rezervavimas</title>
	<!-- demo stylesheet -->
    	<link type="text/css" rel="stylesheet" href="media/layout.css" />

        <link type="text/css" rel="stylesheet" href="themes/calendar_g.css" />
        <link type="text/css" rel="stylesheet" href="themes/calendar_green.css" />
        <link type="text/css" rel="stylesheet" href="themes/calendar_traditional.css" />
        <link type="text/css" rel="stylesheet" href="themes/calendar_transparent.css" />
        <link type="text/css" rel="stylesheet" href="themes/calendar_white.css" />

	<!-- helper libraries -->
	<script src="js/jquery-1.9.1.min.js" type="text/javascript"></script>

	<!-- daypilot libraries -->
        <script src="js/daypilot/daypilot-all.min.js" type="text/javascript"></script>

</head>
<body>
<!--
<div class="space">CSS Theme: 
  <select id="theme">
    <option value="calendar_default">Default</option>
    <option value="calendar_g">Google-Like</option>
    <option value="calendar_green">Green</option>
    <option value="calendar_traditional">Traditional</option>
    <option value="calendar_transparent">Transparent</option>
    <option value="calendar_white">White</option>
  </select>
</div>
-->

<div style="padding-top:0px;padding-left: 50px; padding-right:50px; padding-bottom:50px;">

<?php
echo "<table  width='100%'    border='0' ><tr><td width='157' style='text-align: center; vertical-align: middle;  padding: 0em; background:white;'> <img  alt='Valstybinis patentų biuras' src='media/layout/herbas_nukirptas.jpg' /> 



</td>
<td  style='background: url(media/layout/hed.jpg) no-repeat; color:  brown; background-color: white; vertical-align:  middle; padding: 0.5em;'> 
<p style=' font-size: 150%; font-weight: bold;'> VPB patalpų rezervavimas (testavimas) </p>

<p>
Kalvarijų g. 3, LT-09310 Vilnius <br />
Kodas 188708943<br />
Informacija tel. (8 5) 278 02 90, faksas (8 5) 275 0723<br />
El. paštas info@vpb.gov.lt
</p>



</td></tr></table>\n";
//purple, gray, orange, green, brown, navy
?>


<br><br>




<div style="float:left; width: 160px;">
  <div id="nav"></div>
</div>
<div style="margin-left: 160px;">
  <div id="dp"></div>
</div>

<p>Versiją, kurioje rezervacijos  apsaugomos slaptaždžiu, rasite <a href="../rezervuoti_test">čia.</a>
</p>

<div style="padding-left: 160px;padding-top: 100px;">
<p>Rezervuoti patalpas renginiui galima tiesiog pele braukiant kalendoriuje reikiamas valandas. Reikės įvesti renginio pavadinimą, rezervuojančiojo vardą, rezervuojamą patalpą (pvz., <i>211 k., skaitykla</i> arba <i>posėdžių salė</i>).</p><p>
Galima pele keisti įvestos rezervacijos pradžią ir pabaigą, perkelti ją į kitą vietą arba visiškai ištrinti.  </p>
<p>Taip pat galima rezervuoti patalpas, jei tuo pačiu metu vyksta renginiai skirtingose patalpose:
</p>
<img src="media/layout/sched_pvz.png" /> 
</div>


</div>

<script type="text/javascript">



//https://code.daypilot.org/17910/html5-event-calendar-open-source
  var dp = new DayPilot.Calendar("dp");
  dp.viewType = "Week";
/*
  dp.events.list = [
  {
    "id":"5",
    "text":"Calendar Event 5",
    "start":"2019-05-14T10:30:00",
    "end":"2019-05-14T16:30:00"
  },
  {
    "id":"6",
    "text":"Calendar Event 6",
    "start":"2019-05-13T09:00:00",
    "end":"2019-05-13T14:30:00"
  },
  {
    "id":"7",
    "text":"Calendar Event 7",
    "start":"2019-05-16T12:00:00",
    "end":"2019-05-16T16:00:00"
  }];

*/
 


  var nav = new DayPilot.Navigator("nav");
  nav.showMonths = 3;
  nav.skipMonths = 3;
  nav.selectMode = "week";
//nav.locale =  "lt-lt";
  nav.init();

nav.onTimeRangeSelected = function(args) {
      dp.startDate = args.day;
      dp.update();
      loadEvents();
  };


/*
$(document).ready(function() {
  $("#theme").change(function(e) {
    dp.theme = this.value;
    dp.update();
  });
});  
*/
$(document).ready(function() {
 /*  Default
    "calendar_g"
   "calendar_green"
    "calendar_traditional"
    "calendar_transparent"
    "calendar_white"
 */ 
   // dp.theme = "calendar_white";
    dp.update();
  
}); 

dp.onEventMoved = function (args) {
  $.post("mbackend_move.php",  
    {
        id: args.e.id(),
        newStart: args.newStart.toString(),
        newEnd: args.newEnd.toString()
    }, 
    function() {
        console.log("Moved.");
    });
};

dp.onEventResized = function (args) {
  $.post("mbackend_move.php",  
  {
      id: args.e.id(),
      newStart: args.newStart.toString(),
      newEnd: args.newEnd.toString()
  }, 
  function() {
      console.log("Resized.");
  });
};


  dp.onTimeRangeSelected = function(args) {
                    var renginys = prompt("Renginio pavadinimas:", "Susitikimas su...");

 		if (!renginys  ) { dp.clearSelection(); return;}
		var vieta = prompt("Renginio vieta:", "Skaitykla");
		if ( !vieta  ) { dp.clearSelection(); return;}
		var rezervavo = prompt("Jūsų vardas:", "   "); 

//var today = new Date();
//var time = today.getHours() + ":" + today.getMinutes() + ":" + today.getSeconds();
//var date = today.getFullYear()+'-'+(today.getMonth()+1)+'-'+today.getDate();

 		name = '<b>' + renginys + '</b>  <br />' + vieta + ' (' + rezervavo + ')';
                    dp.clearSelection();
  if (!renginys || !vieta || !rezervavo ) return;
                  //  if (!name) return;
                    var e = new DayPilot.Event({
                        start: args.start,
                        end: args.end, 
                        id: DayPilot.guid(),
                        resource: args.resource,
                        text: name
                    });

                    dp.events.add(e);

                    $.post("mbackend_create.php",
                            {
                                start: args.start.toString(),
                                end: args.end.toString(),
                                name: name,
//info: info
                            },
                            function() {
                                console.log("Created.");
                            });

                };


dp.eventDeleteHandling = "Update";
dp.onEventDelete = function(args) {
    if (!confirm("Ar tikrai ištrinti šį renginį?")) {
      args.preventDefault();
    }
  };
dp.onEventDeleted = function(args) {

  $.post("mbackend_delete.php",
      {
          id: args.e.id()
      },
      function() {
          console.log("Deleted.");
      });
};

//https://doc.daypilot.org/calendar/event-customization/

var localeLT = new DayPilot.Locale(  //https://api.daypilot.org/daypilot-locale-constructor/
'lt-lt',
{
dayNames:['Sekmadienis','Pirmadienis','Antradienis','Trečiadienis','Ketvirtadienis','Penktadienis','Šeštadienis'],
//dayNames: 'Sekmadienis_Pirmadienis_Antradienis_Trečiadienis_Ketvirtadienis_Penktadienis_Šeštadienis'.split('_'),
dayNamesShort: 'Sk_Pr_An_Tr_Kt_Pn_Št'.split('_'),
monthNames: 'Sausis_Vasaris_Kovas_Balandis_Gegužė_Birželis_Liepa_Rupgjūtis_Rugsėjis_Spalis_Lapkritis_Gruodis'.split('_'),
monthNamesShort: 'Sau_Vas_Kov_Bal_Geg_Bir_Lie_Rgp_Rgs_Spa_Lap_Gru'.split('_'),
timePattern: 'hh:mm:tt',
datePattern: 'yyyy-MM-dd',
dateTimePattern:  'yyyy-MM-dd HH:mm',
timeFormat: 'Clock24Hours',
weekStarts: 1
}
);

DayPilot.Locale.register(localeLT); 





dp.locale = "lt-lt";
nav.locale = "lt-lt";
nav.update();

     dp.viewType = "WorkWeek";  //"Week"

dp.businessBeginsHour = 8;
dp.businessEndsHour = 17;
 dp.init();
 loadEvents();

                function loadEvents() {
                    dp.events.load("mbackend_events.php");
                }

</script>




</body>
</html>


