echo "start periodic jobs `date`"

echo "Login"
wget --server-response --delete-after --keep-session-cookies --save-cookies cookies.txt "http://192.168.10.101:8087/sp-bo-ui-backend/j_spring_security_check?username=boadadmin&password=123456" 2>&1 | awk '/^  HTTP/{print $2}'

echo "To Renew"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/toRenew 2>&1 | awk '/^  HTTP/{print $2}'

echo "Expired"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/expired 2>&1 | awk '/^  HTTP/{print $2}'

echo "Lapsed"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/lapsed 2>&1 | awk '/^  HTTP/{print $2}'

echo "Issue certificate"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/issueCertificate 2>&1 | awk '/^  HTTP/{print $2}'

echo "Registration"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/registration 2>&1 | awk '/^  HTTP/{print $2}'

echo "License expired"
wget --server-response --delete-after --load-cookies cookies.txt http://192.168.10.101:8087/sp-bo-ui-backend/scheduled_task/license 2>&1 | awk '/^  HTTP/{print $2}'

#remove cookies
rm /opt/bo/etc/source/cookies.txt

echo "end periodic jobs `date`"

