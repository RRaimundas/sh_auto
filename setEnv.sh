JAVA_HOME=/usr/lib/jvm/java-8-openjdk-amd64
MULE_HOME=/home/fsp/servers/mule-standalone-3.8.1
alias jps=ps -o pid,user,cmd -C java | sed -e 's/\([0-9]\+ *[^ ]*\) *[^ ]* *\([^$]*\)/\1 \2/' -e 's/-c[^ ]* [^ ]* \|-[^ ]* //g'

# Colour configuration
C_RED="\E[31m"
C_GREEN="\E[32m"
C_YELLOW="\E[33m"
C_BLUE="\E[34m"
C_REDBOLD="\E[1;31m"
C_GREENBOLD="\E[1;32m"
C_YELLOWBOLD="\E[1;33m"
C_BLUEBOLD="\E[1;33m"
C_NORMAL="\E[0m"
C_BOLD="\E[1m"

echo -e ""
echo -e ""
echo -e "    ${C_BLUE}--- ENVIRONMENT VARIABLES ---"
echo -e "=============================================="

export JAVA_HOME
echo -e "${C_GREEN}JAVA_HOME${C_NORMAL} = ${C_YELLOW}$JAVA_HOME${C_NORMAL}"

export MULE_HOME
echo -e "${C_GREEN}MULE_HOME${C_NORMAL} = ${C_YELLOW}$MULE_HOME${C_NORMAL}"

#PATH=$PATH:$JAVA_HOME/bin

echo -e ""
echo -e "    ${C_BLUE}--- ALIAS DEFINITIONS ---"
echo -e "==============================================${C_NORMAL}"
alias mhome='cd $MULE_HOME'
alias startmule='${MULE_HOME}/bin/mule start -M-Dmule.verbose.exceptions=true'
alias stopmule='${MULE_HOME}/bin/mule stop'
alias tailbo='tail -5000f ${MULE_HOME}/logs/bo-pdb-export.log'
alias tailds='tail -5000f ${MULE_HOME}/logs/dsview.log'
alias tailtm='tail -5000f ${MULE_HOME}/logs/tmview.log'
alias tailmule='tail -500f ${MULE_HOME}/logs/mule.log'
alias grepmule='ps aux | grep mule'
alias startseniorties='cd /home/fsp/seniorities && nohup ./schedule.sh &'

echo -e "Added the following alias:"
echo -e "   - Mule Home........${C_GREEN}mhome${C_NORMAL}"
echo -e "   - Mule Start.......${C_GREEN}startmule${C_NORMAL}"
echo -e "   - Mule Stop........${C_GREEN}stopmule${C_NORMAL}"
echo -e "   - Mule Autostart runs with OS init.sh...check mule pid with added alias jps"



echo -e "${C_BLUE}"
echo -e "================================================="
echo -e "================= WARNNIG =======================${C_NORMAL}"
echo -e " Run this script everytime you enter a new shell"
echo -e "           ${C_GREEN}$> source ./servers/setEnv.sh${C_BLUE}"
echo -e "=================================================${C_NORMAL}"

echo -e ""
echo -e ""

export HISTSIZE=20000
shopt -s histappend
